import { Component, OnInit } from '@angular/core';
import Productdata from './product_data.json';

interface product {    
  p_name?: any;  
  p_id?: any;
  p_cost?: any;  
  p_availability?: any; 
  p_details?: any;
  p_category?: any;
  p_image?: any;
}


@Component({
  selector: 'app-shop',
  templateUrl: './shop.component.html',
  styleUrls: ['./shop.component.css']
})
export class ShopComponent implements OnInit {
  name = 'Angular';  
  productArray: product[] = Productdata;
  products: any;
  category:any;
  
  constructor() { }

  ngOnInit(): void {
    console.log('products', this.productArray);
    this.filterData('All')
  }
  /**to sort by category using category name */
  filterData(category: any) {
    this.products = Productdata.filter(object => {
      if (category == 'All') {
        return object['p_name'];
      } else {
        return object['p_category'] == category;
      }
    }); 
  }
}
